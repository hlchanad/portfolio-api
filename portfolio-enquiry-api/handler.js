'use strict';

const axios = require('axios');

axios.defaults.baseURL = process.env.EMAIL_SERVER_BASE_URL

module.exports.send = async (event, context) => {
  console.log('event', event);
    
  const body = JSON.parse(event.body);
  console.log('body', body);
  
  if (!body.name || !body.email || !body.message) {
    return {
      statusCode: 400,
      body: 'Missing required parameters',
    };
  }


  let response;
  try {
    response = await axios.post('/send', {
      toEmails: [ process.env.TO_EMAIL ],
      subject: 'hlchanad-portfolio - New Enquiry Received',
      message: `
        <p>name: ${ body.name }</p>
        <p>email: ${ body.email }</p>
        <p>message: ${ body.message }</p>
      `,
    });
  } catch (err) {
    console.error('send email error', err.message);
    if (err.response && err.response.data) {
      console.error(err.response.data)
    }
    
    return {
      statusCode: 503,
      body: 'Email Service Unavailable',
    };
  }
  
  console.log(response.data);
  
  return {
    statusCode: 200,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers' : 'Content-Type',
      'Access-Control-Allow-Methods': 'OPTIONS,POST',
      'Access-Control-Allow-Credentials': true,
    },
    body: 'ok',
  };
};
